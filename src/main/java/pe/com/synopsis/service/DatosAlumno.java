package pe.com.synopsis.service;

import java.util.List;

import pe.com.synopsis.beans.Alumno;

public interface DatosAlumno {

	public Alumno obtenerDatosAlumno();
	
	public List<Alumno> obtenerListaDeAlumnos();
}
