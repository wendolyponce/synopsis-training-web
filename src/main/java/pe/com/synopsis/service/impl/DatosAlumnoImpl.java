package pe.com.synopsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import pe.com.synopsis.beans.Alumno;
import pe.com.synopsis.service.DatosAlumno;

@Service
public class DatosAlumnoImpl implements DatosAlumno {

	@Override
	public Alumno obtenerDatosAlumno() 
	{
		Alumno a = new Alumno("alberto", "maldonado", 36);
		return a;
	}

	@Override
	public List<Alumno> obtenerListaDeAlumnos() {
		
		List<Alumno> listaAlumnos =  new ArrayList<Alumno>();
		
		Alumno a = new Alumno("alberto", "maldonado", 36);
		listaAlumnos.add(a);
		
		Alumno a1 = new Alumno("luis", "sanchez", 25);
		listaAlumnos.add(a1);
		
		Alumno a2 = new Alumno("Carla", "Ramos", 30);
		listaAlumnos.add(a2);
		
		return listaAlumnos;
	}

}
