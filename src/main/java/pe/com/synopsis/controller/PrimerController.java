package pe.com.synopsis.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import pe.com.synopsis.service.DatosAlumno;

@Controller
public class PrimerController 
{
	private static final Logger logger = LogManager.getLogger(PrimerController.class);
	
	@Autowired
	DatosAlumno datosAlumno;
	
	@GetMapping("/")
	public String primerMetodo(Model model)
	{
		try
		{
			model.addAttribute("alumno", datosAlumno.obtenerDatosAlumno());
			System.out.println("ingreso al primer metodo");
			
			logger.info("Ingreso al metodo primerMetodo");
		
		}
		catch (Exception e) 
		{
			logger.error("se genero un error:"+e);
		}
		return "index";
	}
	
	
	@GetMapping("/dos") 
	public String segundoMetodo(Model model)
	{
		logger.info("Ingreso al metodo segundoMetodo");
		model.addAttribute("listaAlumnos", datosAlumno.obtenerListaDeAlumnos());
		return "SegundaPaginaHtml";
	}
}
